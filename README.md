# Incubyte_Data_Engineer_assignment

# working 
1.Firstly MySQL database has been created with specified schema.

2.'program_mysql.connect.py' python script, fetches database by establishing connection with MySQL server.

3.The retrieved data is fitted into pandas dataframe for further table manipulation.

4.Taking input from user for which country's customers data to be fetched and giving that input as a parameter to getRecords() & printRecords() and then these functions are called to fetch the desired country rows and generating .csv file formats to specified path.
